package com.gitlab.antcrepin.raytracing.utils;

import javax.vecmath.Vector3d;

public class ArgumentChecker {

    private static final String ARGUMENT = "Argument ";

    private ArgumentChecker() {}

    public static void checkNotNullArgument(Object object, String description) {
        if (object == null) {
            throw new IllegalArgumentException(ARGUMENT + description + " cannot be null");
        }
    }

    public static void checkNonZeroVector(Vector3d vector3d, String description) {
        if (vector3d.length() == 0.) {
            throw new IllegalArgumentException(ARGUMENT + description + " cannot be the zero vector");
        }
    }

    public static void checkVectorOrthogonality(
            Vector3d vector1,
            String description1,
            Vector3d vector2,
            String description2
    ) {
        if (vector1.dot(vector2) != 0.) {
            throw new IllegalArgumentException(
                    "Vectors " + description1 + " and " + description2 + " must be orthogonal"
            );
        }
    }

    public static void checkBoundedArgument(double value, double lb, double ub, String description) {
        if (value < lb || value > ub) {
            throw new IllegalArgumentException(ARGUMENT + description + " should be between " + lb + " and " + ub);
        }
    }

    public static void checkBoundedArgument(int value, int lb, int ub, String description) {
        if (value < lb || value > ub) {
            throw new IllegalArgumentException(ARGUMENT + description + " should be between " + lb + " and " + ub);
        }
    }

    public static void checkPositiveArgument(double value, String description) {
        if (value <= 0.) {
            throw new IllegalArgumentException(ARGUMENT + description + " should be positive");
        }
    }

    public static void checkPositiveArgument(int value, String description) {
        if (value <= 0) {
            throw new IllegalArgumentException(ARGUMENT + description + " should be positive");
        }
    }
}
