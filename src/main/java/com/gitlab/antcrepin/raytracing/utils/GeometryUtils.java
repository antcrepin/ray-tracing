package com.gitlab.antcrepin.raytracing.utils;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Arrays;

public class GeometryUtils {

    private GeometryUtils() {}

    public static Vector3d normalize(Vector3d vector3d) {
        var copy = new Vector3d(vector3d);
        copy.normalize();
        return copy;
    }

    public static Vector3d getVector(Point3d from, Point3d to) {
        var vector3d = new Vector3d();
        vector3d.sub(to, from);
        return vector3d;
    }

    public static Point3d addVector(Point3d initial, Vector3d shift) {
        var point3d = new Point3d();
        point3d.add(initial, shift);
        return point3d;
    }

    public static Vector3d scale(Vector3d vector3d, double factor) {
        var copy = new Vector3d(vector3d);
        copy.scale(factor);
        return copy;
    }

    public static Vector3d crossProduct(Vector3d vector1, Vector3d vector2) {
        var vector3d = new Vector3d();
        vector3d.cross(vector1, vector2);
        return vector3d;
    }

    public static Vector3d sum(Vector3d... vectors) {
        var sum = new Vector3d();
        Arrays.stream(vectors).forEach(sum::add);
        return sum;
    }
}
