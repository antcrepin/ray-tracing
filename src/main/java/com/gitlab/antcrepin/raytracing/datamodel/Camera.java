package com.gitlab.antcrepin.raytracing.datamodel;

import com.gitlab.antcrepin.raytracing.utils.ArgumentChecker;
import com.gitlab.antcrepin.raytracing.utils.GeometryUtils;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class Camera {

    private final int imageNbColumns;
    private final int imageNbRows;
    private final double dotsPerUnit;
    private final double focalLength;

    private final Point3d position;
    private final Vector3d rollDirection;
    private final Vector3d yawDirection;

    public Camera(
            int imageNbColumns,
            int imageNbRows,
            double dotsPerUnit,
            double focalLength,
            Point3d position,
            Vector3d rollDirection,
            Vector3d yawDirection
    ) {
        ArgumentChecker.checkPositiveArgument(imageNbColumns, "Camera::imageNbColumns");
        this.imageNbColumns = imageNbColumns;
        ArgumentChecker.checkPositiveArgument(imageNbRows, "Camera::imageNbRows");
        this.imageNbRows = imageNbRows;
        ArgumentChecker.checkPositiveArgument(dotsPerUnit, "Camera::dotsPerUnit");
        this.dotsPerUnit = dotsPerUnit;
        ArgumentChecker.checkPositiveArgument(focalLength, "Camera::focalLength");
        this.focalLength = focalLength;
        ArgumentChecker.checkNotNullArgument(position, "Camera::position");
        this.position = position;
        ArgumentChecker.checkNotNullArgument(rollDirection, "Camera::rollDirection");
        ArgumentChecker.checkNonZeroVector(rollDirection, "Camera::rollDirection");
        this.rollDirection = GeometryUtils.normalize(rollDirection);
        ArgumentChecker.checkNotNullArgument(yawDirection, "Camera::yawDirection");
        ArgumentChecker.checkNonZeroVector(yawDirection, "Camera::yawDirection");
        this.yawDirection = GeometryUtils.normalize(yawDirection);
        ArgumentChecker.checkVectorOrthogonality(
                rollDirection,
                "Camera::rollDirection",
                yawDirection,
                "Camera::yawDirection"
        );
    }

    public int getImageNbColumns() {
        return imageNbColumns;
    }

    public int getImageNbRows() {
        return imageNbRows;
    }

    public double getDotsPerUnit() {
        return dotsPerUnit;
    }

    public double getFocalLength() {
        return focalLength;
    }

    public Point3d getPosition() {
        return position;
    }

    public Vector3d getRollDirection() {
        return rollDirection;
    }

    public Vector3d getYawDirection() {
        return yawDirection;
    }

    /**
     * Build a ray coming from the camera and going to a given pixel of the image
     * @return a ray coming from the camera and going to a given pixel of the image
     */
    public Ray rayAt(int column, int row) {
        ArgumentChecker.checkBoundedArgument(column, 1, imageNbColumns, "Ray::rayAt::column");
        ArgumentChecker.checkBoundedArgument(row, 1, imageNbRows, "Ray::rayAt::row");
        Vector3d rollComponent = GeometryUtils.scale(rollDirection, focalLength);
        Vector3d pitchComponent = GeometryUtils.scale(
                GeometryUtils.crossProduct(rollDirection, yawDirection),
                column - (imageNbColumns + 1) / 2.
        );
        Vector3d yawComponent = GeometryUtils.scale(yawDirection, (imageNbRows + 1) / 2. - row);
        return new Ray(position, GeometryUtils.sum(rollComponent, pitchComponent, yawComponent));
    }
}
