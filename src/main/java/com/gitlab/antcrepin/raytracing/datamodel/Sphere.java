package com.gitlab.antcrepin.raytracing.datamodel;

import com.gitlab.antcrepin.raytracing.utils.ArgumentChecker;
import com.gitlab.antcrepin.raytracing.utils.GeometryUtils;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Optional;

public class Sphere extends SceneObject {

    private final Point3d centerPosition;
    private final double radius;

    public Sphere(Point3d centerPosition, double radius, Material material) {
        super(material);
        ArgumentChecker.checkNotNullArgument(centerPosition, "Sphere::centerPosition");
        this.centerPosition = centerPosition;
        ArgumentChecker.checkPositiveArgument(radius, "Sphere::radius");
        this.radius = radius;
    }

    public Point3d getCenterPosition() {
        return centerPosition;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public Optional<Intersection> getIntersectionWithRay(Ray ray) {
        Vector3d sphereCenterToRayOrigin = GeometryUtils.getVector(centerPosition, ray.getStartingPoint());
        boolean rayOriginInsideSphere = sphereCenterToRayOrigin.length() < radius;
        double b = 2 * ray.getDirection().dot(sphereCenterToRayOrigin);
        double c = sphereCenterToRayOrigin.lengthSquared() - Math.pow(radius, 2);
        double discriminant = Math.pow(b, 2) - 4 * c;
        Double distanceFromRayOrigin = null;
        if (discriminant >= 0.) {
            double solution1 = (-b - Math.sqrt(discriminant)) / 2;
            double solution2 = (-b + Math.sqrt(discriminant)) / 2;
            if (solution1 >= 0.) {
                distanceFromRayOrigin = solution1;
            } else if (solution2 >= 0.){
                distanceFromRayOrigin = solution2;
            }
        }
        return Optional
                .ofNullable(distanceFromRayOrigin)
                .map(d -> {
                    Point3d intersectionPosition = GeometryUtils.addVector(
                            ray.getStartingPoint(),
                            GeometryUtils.scale(ray.getDirection(), d)
                    );
                    return new Intersection(
                            intersectionPosition,
                            GeometryUtils.scale(
                                    GeometryUtils.getVector(centerPosition, intersectionPosition),
                                    rayOriginInsideSphere ? -1. : 1.
                            )
                    );
                });
    }
}
