package com.gitlab.antcrepin.raytracing.datamodel;

import com.gitlab.antcrepin.raytracing.utils.ArgumentChecker;
import com.gitlab.antcrepin.raytracing.utils.GeometryUtils;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class Ray {

    private final Point3d startingPoint;
    private final Vector3d direction;

    public Ray(Point3d startingPoint, Vector3d direction) {
        ArgumentChecker.checkNotNullArgument(startingPoint, "Ray::startingPoint");
        this.startingPoint = startingPoint;
        ArgumentChecker.checkNotNullArgument(direction, "Ray::direction");
        ArgumentChecker.checkNonZeroVector(direction, "Ray::direction");
        this.direction = GeometryUtils.normalize(direction);
    }

    public Point3d getStartingPoint() {
        return startingPoint;
    }

    public Vector3d getDirection() {
        return direction;
    }
}
