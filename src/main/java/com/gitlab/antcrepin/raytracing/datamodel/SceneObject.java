package com.gitlab.antcrepin.raytracing.datamodel;

import com.gitlab.antcrepin.raytracing.utils.ArgumentChecker;
import java.util.Optional;

public abstract class SceneObject {

    private final Material material;

    protected SceneObject(Material material) {
        ArgumentChecker.checkNotNullArgument(material, "SceneObject::material");
        this.material = material;
    }

    public Material getMaterial() {
        return material;
    }

    public abstract Optional<Intersection> getIntersectionWithRay(Ray ray);
}
