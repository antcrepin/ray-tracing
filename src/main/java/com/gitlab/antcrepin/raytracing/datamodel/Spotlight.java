package com.gitlab.antcrepin.raytracing.datamodel;

import com.gitlab.antcrepin.raytracing.utils.ArgumentChecker;
import javax.vecmath.Point3d;
import java.awt.Color;

public class Spotlight {

    private final Point3d position;
    private final Color color;

    public Spotlight(Point3d position, Color color) {
        ArgumentChecker.checkNotNullArgument(position, "Spotlight::position");
        this.position = position;
        ArgumentChecker.checkNotNullArgument(color, "Spotlight::color");
        this.color = color;
    }

    public Point3d getPosition() {
        return position;
    }

    public Color getColor() {
        return color;
    }
}
