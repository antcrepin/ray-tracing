package com.gitlab.antcrepin.raytracing.datamodel;

import com.gitlab.antcrepin.raytracing.utils.ArgumentChecker;
import com.gitlab.antcrepin.raytracing.utils.GeometryUtils;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class Intersection {

    private final Point3d position;
    private final Vector3d normal;

    public Intersection(Point3d position, Vector3d normal) {
        ArgumentChecker.checkNotNullArgument(position, "Intersection::position");
        this.position = position;
        ArgumentChecker.checkNotNullArgument(normal, "Intersection::normal");
        ArgumentChecker.checkNonZeroVector(normal, "Intersection::normal");
        this.normal = GeometryUtils.normalize(normal);
    }

    public Point3d getPosition() {
        return position;
    }

    public Vector3d getNormal() {
        return normal;
    }
}
