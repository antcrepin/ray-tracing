package com.gitlab.antcrepin.raytracing.datamodel;

import com.gitlab.antcrepin.raytracing.utils.ArgumentChecker;
import java.awt.Color;

public class Material {

    private final Color color;

    private final double ambient;
    private final double diffuse;
    private final double specular;
    private final double shininess;
    private final double reflection;

    public Material(
            Color color,
            double ambient,
            double diffuse,
            double specular,
            double shininess,
            double reflection
    ) {
        ArgumentChecker.checkNotNullArgument(color, "Material::color");
        this.color = color;
        ArgumentChecker.checkBoundedArgument(ambient, 0., 1., "Material::ambient");
        this.ambient = ambient;
        ArgumentChecker.checkBoundedArgument(diffuse, 0., 1., "Material::diffuse");
        this.diffuse = diffuse;
        ArgumentChecker.checkBoundedArgument(specular, 0., 1., "Material::specular");
        this.specular = specular;
        ArgumentChecker.checkBoundedArgument(shininess, 0., 50., "Material::shininess");
        this.shininess = shininess;
        ArgumentChecker.checkBoundedArgument(reflection, 0., 1., "Material::reflection");
        this.reflection = reflection;
    }

    public Color getColor() {
        return color;
    }

    public double getAmbient() {
        return ambient;
    }

    public double getDiffuse() {
        return diffuse;
    }

    public double getSpecular() {
        return specular;
    }

    public double getShininess() {
        return shininess;
    }

    public double getReflection() {
        return reflection;
    }
}
