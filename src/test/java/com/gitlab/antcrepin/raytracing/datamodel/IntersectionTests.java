package com.gitlab.antcrepin.raytracing.datamodel;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IntersectionTests {

    @Test
    void test() {
        var origin = new Point3d(0, 0, 0);
        var zeroElement = new Vector3d(0, 0, 0);
        var nonUnitNormal = new Vector3d(0, 0, 2);

        Assertions.assertThrows(IllegalArgumentException.class, () -> new Intersection(null, nonUnitNormal));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Intersection(origin, null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Intersection(origin, zeroElement));

        var ray = new Intersection(origin, nonUnitNormal);
        Assertions.assertEquals(origin, ray.getPosition());
        Assertions.assertEquals(nonUnitNormal.length(), ray.getNormal().dot(nonUnitNormal));
    }
}
