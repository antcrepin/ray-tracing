package com.gitlab.antcrepin.raytracing.datamodel;

import javax.vecmath.Point3d;
import java.awt.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SpotlightTests {

    @Test
    void test() {
        var origin = new Point3d(0, 0, 0);

        Assertions.assertThrows(IllegalArgumentException.class, () -> new Spotlight(null, Color.WHITE));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Spotlight(origin, null));

        var spotlight = new Spotlight(origin, Color.WHITE);
        Assertions.assertEquals(origin, spotlight.getPosition());
        Assertions.assertEquals(Color.WHITE, spotlight.getColor());
    }
}
