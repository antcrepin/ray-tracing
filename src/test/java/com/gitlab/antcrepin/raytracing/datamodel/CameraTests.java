package com.gitlab.antcrepin.raytracing.datamodel;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CameraTests {

    @Test
    void test() {
        int imageNbColumns = 1920;
        int imageNbRows = 1080;
        double dotsPerUnit = 200 / .0254;
        double focalLength = .030;
        var origin = new Point3d(0, 0, 0);
        var zeroElement = new Vector3d(0, 0, 0);
        var nonUnitRollDirection = new Vector3d(2, 0, 0);
        var nonUnitYawDirection = new Vector3d(0, 0, 3);

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        0,
                        imageNbRows,
                        dotsPerUnit,
                        focalLength,
                        origin,
                        nonUnitRollDirection,
                        nonUnitYawDirection
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        0,
                        dotsPerUnit,
                        focalLength,
                        origin,
                        nonUnitRollDirection,
                        nonUnitYawDirection
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        imageNbRows,
                        0.,
                        focalLength,
                        origin,
                        nonUnitRollDirection,
                        nonUnitYawDirection
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        imageNbRows,
                        dotsPerUnit,
                        0.,
                        origin,
                        nonUnitRollDirection,
                        nonUnitYawDirection
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        imageNbRows,
                        dotsPerUnit,
                        focalLength,
                        null,
                        nonUnitRollDirection,
                        nonUnitYawDirection
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        imageNbRows,
                        dotsPerUnit,
                        focalLength,
                        origin,
                        null,
                        nonUnitYawDirection
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        imageNbRows,
                        dotsPerUnit,
                        focalLength,
                        origin,
                        zeroElement,
                        nonUnitYawDirection
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        imageNbRows,
                        dotsPerUnit,
                        focalLength,
                        origin,
                        nonUnitRollDirection,
                        null
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        imageNbRows,
                        dotsPerUnit,
                        focalLength,
                        origin,
                        nonUnitRollDirection,
                        zeroElement
                )
        );
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Camera(
                        imageNbColumns,
                        imageNbRows,
                        dotsPerUnit,
                        focalLength,
                        origin,
                        nonUnitRollDirection,
                        nonUnitRollDirection
                )
        );

        var camera = new Camera(
                imageNbColumns,
                imageNbRows,
                dotsPerUnit,
                focalLength,
                origin,
                nonUnitRollDirection,
                nonUnitYawDirection
        );
        Assertions.assertNotNull(camera);
        Assertions.assertEquals(imageNbColumns, camera.getImageNbColumns());
        Assertions.assertEquals(imageNbRows, camera.getImageNbRows());
        Assertions.assertEquals(dotsPerUnit, camera.getDotsPerUnit());
        Assertions.assertEquals(focalLength, camera.getFocalLength());
        Assertions.assertEquals(origin, camera.getPosition());
        Assertions.assertEquals(nonUnitRollDirection.length(), camera.getRollDirection().dot(nonUnitRollDirection));
        Assertions.assertEquals(nonUnitYawDirection.length(), camera.getYawDirection().dot(nonUnitYawDirection));
    }
}
