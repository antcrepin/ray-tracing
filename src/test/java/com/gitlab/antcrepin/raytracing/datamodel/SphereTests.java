package com.gitlab.antcrepin.raytracing.datamodel;

import javax.vecmath.Point3d;
import java.awt.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SphereTests {

    @Test
    void test() {
        var origin = new Point3d(0, 0, 0);
        var material = new Material(Color.BLUE, .1, .2, .3, .4, .5);

        Assertions.assertThrows(IllegalArgumentException.class, () -> new Sphere(null, 1., material));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Sphere(origin, 0., material));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Sphere(origin, 1., null));

        var sphere = new Sphere(origin, 1., material);
        Assertions.assertEquals(origin, sphere.getCenterPosition());
        Assertions.assertEquals(1., sphere.getRadius());
        Assertions.assertEquals(material, sphere.getMaterial());
    }
}
