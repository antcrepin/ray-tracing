package com.gitlab.antcrepin.raytracing.datamodel;

import java.awt.Color;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MaterialTests {

    @Test
    void test() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(null, .1, .2, .3, .4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, -.1, .2, .3, .4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, 1.1, .2, .3, .4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, .1, -.2, .3, .4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, .1, 2.2, .3, .4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, .1, .2, -.3, .4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, .1, .2, 3.3, .4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, .1, .2, .3, -.4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, .1, .2, .3, 50.4, .5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, .1, .2, .3, .4, -.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Material(Color.BLUE, .1, .2, .3, .4, 1.5));

        var material = new Material(Color.BLUE, .1, .2, .3, .4, .5);
        Assertions.assertEquals(Color.BLUE, material.getColor());
        Assertions.assertEquals(.1, material.getAmbient());
        Assertions.assertEquals(.2, material.getDiffuse());
        Assertions.assertEquals(.3, material.getSpecular());
        Assertions.assertEquals(.4, material.getShininess());
        Assertions.assertEquals(.5, material.getReflection());
    }
}
