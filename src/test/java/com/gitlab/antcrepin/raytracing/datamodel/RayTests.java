package com.gitlab.antcrepin.raytracing.datamodel;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RayTests {

    @Test
    void test() {
        var origin = new Point3d(0, 0, 0);
        var zeroElement = new Vector3d(0, 0, 0);
        var nonUnitDirection = new Vector3d(0, 0, 2);

        Assertions.assertThrows(IllegalArgumentException.class, () -> new Ray(null, nonUnitDirection));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Ray(origin, null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Ray(origin, zeroElement));

        var ray = new Ray(origin, nonUnitDirection);
        Assertions.assertEquals(origin, ray.getStartingPoint());
        Assertions.assertEquals(nonUnitDirection.length(), ray.getDirection().dot(nonUnitDirection));
    }
}
