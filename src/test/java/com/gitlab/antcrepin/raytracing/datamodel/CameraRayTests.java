package com.gitlab.antcrepin.raytracing.datamodel;

import com.gitlab.antcrepin.raytracing.utils.GeometryUtils;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CameraRayTests {

    @Test
    void test() {
        int imageNbColumns = 1919;
        int imageNbRows = 1079;
        double dotsPerUnit = 200 / .0254;
        double focalLength = .030;
        var origin = new Point3d(0, 0, 0);
        var rollDirection = new Vector3d(1, 0, 0);
        var yawDirection = new Vector3d(0, 0, 1);

        // if the numbers of rows and columns are odd, the ray should be in the roll direction
        var camera0 = new Camera(
                imageNbColumns,
                imageNbRows,
                dotsPerUnit,
                focalLength,
                origin,
                rollDirection,
                yawDirection
        );
        Assertions.assertThrows(IllegalArgumentException.class, () -> camera0.rayAt(0, 540));
        Assertions.assertThrows(IllegalArgumentException.class, () -> camera0.rayAt(2000, 540));
        Assertions.assertThrows(IllegalArgumentException.class, () -> camera0.rayAt(960, 0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> camera0.rayAt(960, 1100));
        Ray ray = camera0.rayAt(960, 540);
        Assertions.assertEquals(origin, ray.getStartingPoint());
        Assertions.assertEquals(rollDirection, ray.getDirection());

        // the dot (1, 1) corresponds to the top left corner of the image
        imageNbColumns = 1920;
        imageNbRows = 1080;
        var camera = new Camera(
                imageNbColumns,
                imageNbRows,
                dotsPerUnit,
                focalLength,
                origin,
                rollDirection,
                yawDirection
        );
        ray = camera.rayAt(1, 1);
        var expectedDirection = new Vector3d(focalLength, (imageNbColumns - 1) / 2., (imageNbRows - 1) / 2.);
        Assertions.assertEquals(origin, ray.getStartingPoint());
        Assertions.assertEquals(GeometryUtils.normalize(expectedDirection), ray.getDirection());

        // the dot (imageNbColumns, imageNbRows) corresponds to the bottom right corner of the image
        ray = camera.rayAt(imageNbColumns, imageNbRows);
        expectedDirection = new Vector3d(focalLength, (1 - imageNbColumns) / 2., (1 - imageNbRows) / 2.);
        Assertions.assertEquals(origin, ray.getStartingPoint());
        Assertions.assertEquals(GeometryUtils.normalize(expectedDirection), ray.getDirection());
    }
}
