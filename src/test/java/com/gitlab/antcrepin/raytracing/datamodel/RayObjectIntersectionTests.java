package com.gitlab.antcrepin.raytracing.datamodel;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.awt.Color;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RayObjectIntersectionTests {

    @Test
    void testRaySphereIntersection() {
        var sphereCenter = new Point3d(0., 0., 3.);
        var sphereMaterial = new Material(Color.BLUE, .1, .2, .3, .4, .5);
        var sphere = new Sphere(sphereCenter, 1., sphereMaterial);

        // ray starting right below the sphere
        var rayOrigin = new Point3d(0., 0., 0.);
        var rayDirection = new Vector3d(0., 0., 1.);
        var ray = new Ray(rayOrigin, rayDirection);
        var expectedIntersectionPosition = new Point3d(0., 0., 2.);
        var expectedIntersectionNormal = new Vector3d(0., 0., -1.);
        Optional<Intersection> intersection = sphere.getIntersectionWithRay(ray);
        Assertions.assertTrue(intersection.isPresent());
        Assertions.assertEquals(expectedIntersectionPosition, intersection.get().getPosition());
        Assertions.assertEquals(expectedIntersectionNormal, intersection.get().getNormal());

        // ray starting right above the sphere
        rayOrigin = new Point3d(0., 0., 10.);
        rayDirection = new Vector3d(0., 0., -1.);
        ray = new Ray(rayOrigin, rayDirection);
        expectedIntersectionPosition = new Point3d(0., 0., 4.);
        expectedIntersectionNormal = new Vector3d(0., 0., 1.);
        intersection = sphere.getIntersectionWithRay(ray);
        Assertions.assertTrue(intersection.isPresent());
        Assertions.assertEquals(expectedIntersectionPosition, intersection.get().getPosition());
        Assertions.assertEquals(expectedIntersectionNormal, intersection.get().getNormal());

        // ray starting inside the sphere
        rayOrigin = new Point3d(0., 0., 3.);
        rayDirection = new Vector3d(0., 0., -1.);
        ray = new Ray(rayOrigin, rayDirection);
        expectedIntersectionPosition = new Point3d(0., 0., 2.);
        expectedIntersectionNormal = new Vector3d(0., 0., 1.);
        intersection = sphere.getIntersectionWithRay(ray);
        Assertions.assertTrue(intersection.isPresent());
        Assertions.assertEquals(expectedIntersectionPosition, intersection.get().getPosition());
        Assertions.assertEquals(expectedIntersectionNormal, intersection.get().getNormal());

        // ray not intersecting the sphere
        rayOrigin = new Point3d(0., 0., 5.);
        rayDirection = new Vector3d(0., 0., 1.);
        ray = new Ray(rayOrigin, rayDirection);
        intersection = sphere.getIntersectionWithRay(ray);
        Assertions.assertTrue(intersection.isEmpty());
    }
}
